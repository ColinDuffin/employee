import React from "react";
import { Link } from "react-router-dom";

const HomePage = () => (
  <div className="jumbotron">
    <h1>Customer Relationship Managment</h1>
    <p>Authorization Component Can Go Here</p>
    <Link to="about" className="btn btn-primary btn-lg">
      Log In
    </Link>
  </div>
);

export default HomePage;
