import React from "react";
import { connect } from "react-redux";
import * as courseActions from "../../redux/actions/courseActions";
import * as authorActions from "../../redux/actions/authorActions";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import CustomerList from "./CustomerList";
import { Redirect } from "react-router-dom";

class CustomersPage extends React.Component {
  state = {
    redirectToAddCustomerPage: false,
  };

  componentDidMount() {
    const { courses, authors, actions } = this.props;

    if (courses.length === 0) {
      actions.loadCourses().catch((error) => {
        alert("Loading customers failed" + error);
      });
    }

    if (authors.length === 0) {
      actions.loadAuthors().catch((error) => {
        alert("Loading history failed" + error);
      });
    }
  }

  render() {
    return (
      <>
        {this.state.redirectToAddCustomerPage && <Redirect to="/course" />}
        <h2>Customers</h2>
        <button
          className="btn btn-secondary"
          onClick={() => this.setState({ redirectToAddCustomerPage: true })}
        >
          Add Customer )
        </button>
        <div>(Todo: make this a nicer ui)</div>
        <CustomerList courses={this.props.courses} />
      </>
    );
  }
}

CustomersPage.propTypes = {
  authors: PropTypes.array.isRequired,
  courses: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    courses:
      state.authors.length === 0
        ? []
        : state.courses.map((course) => {
            return {
              ...course,
              authorName: state.authors.find((a) => a.id === course.authorId)
                .name,
            };
          }),
    authors: state.authors,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      loadCourses: bindActionCreators(courseActions.loadCourses, dispatch),
      loadAuthors: bindActionCreators(authorActions.loadAuthors, dispatch),
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomersPage);
